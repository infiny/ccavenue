var http = require('http'),
    fs = require('fs'),
    ccav = require('./ccavutil.js'),
    qs = require('querystring');

exports.postReq = function(request,response){
    var body = '',
	workingKey = 'BFF6C01385D8C36EE6D6EDC74393BC41',	//Put in the 32-Bit key shared by CCAvenues.
	accessCode = 'AVLG91HD82CJ75GLJC',			//Put in the Access Code shared by CCAvenues.
	encRequest = '',  
	formbody = '';
				
    request.on('data', function (data) {
    body += data;
    console.log(body)
    console.log('workingKey --- >'+workingKey)
	encRequest = ccav.encrypt(body,workingKey); 
	formbody = '<form id="nonseamless" method="post" name="redirect" action="https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction"/> <input type="hidden" id="encRequest" name="encRequest" value="' + encRequest + '"><input type="hidden" name="access_code" id="access_code" value="' + accessCode + '"><script language="javascript">document.redirect.submit();</script></form>';
    });
				
    request.on('end', function () {
        response.writeHeader(200, {"Content-Type": "text/html"});
	response.write(formbody);
	response.end();
    });
   return; 
};
